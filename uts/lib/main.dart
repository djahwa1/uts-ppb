import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Biodata Diri',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Biodata(),
    );
  }
}

class Biodata extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.brown[700],
        title: Center(
          child: Text(
            'Profil Mahasiswa',
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.all(10),
            height: 700,
            width: 700,
            decoration: (BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: Colors.amber,
            )),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 50),
                  width: 150,
                  height: 210,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(90),
                    child: Image(
                      image: AssetImage('images/profil.jpg'),
                    ),
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.all(20),
                          height: 30,
                          width: 350,
                          child: Text('BIODATA MAHASISWA',
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                              color: Colors.blue[400]),
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'NPM                       :       031200053',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          decoration: BoxDecoration(color: Colors.blue[50]),
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'Nama                     :       Mgs Djahwa Haridsyah',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          decoration: BoxDecoration(color: Colors.blue[400]),
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'Prodi                      :       D3 Sistem Informasi',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          decoration: BoxDecoration(color: Colors.blue[50]),
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'No HP                    :      081373769161',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          decoration: BoxDecoration(color: Colors.blue[400]),
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                              'TTL                        :       Palembang,17 Juni 2002',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          decoration: BoxDecoration(color: Colors.blue[50]),
                          padding: EdgeInsets.only(left: 40),
                          child: Text('Jenis Kelamin      :       Laki-laki',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 30,
                          width: 350,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                              color: Colors.blue[400]),
                          padding: EdgeInsets.only(left: 40),
                          child: Text('Agama                  :       Islam',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ],
                    ))
              ],
            )),
      ),
    );
  }
}
